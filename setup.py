#!/usr/bin/env python

# Copyright © 2015 Adam Seyfarth <aseyfarth@aseyfarth.name>
#
# This file is part of Intervallic.
#
# Intervallic is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Intervallic is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Intervallic.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup

setup(
    name="intervallic",
    version="0.0.1",
    py_modules=["gather"],
    install_requires=[
        "numpy",
        "pandas",
        "music21",
        "tqdm",
    ],
    entry_points="""
    [console_scripts]
    gather=gather:main
    """
)
