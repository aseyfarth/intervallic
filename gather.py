import sys
import argparse
import functools as fts
import numpy as np
import pandas as pd
from tqdm import tqdm
import music21

def main():
    args = command_line()
    data = intervals(args.n)
    data.to_csv(args.output)

def command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument("output", nargs="?", type=argparse.FileType("w"),
                        default=sys.stdout)
    parser.add_argument("-n", type=int)
    args = parser.parse_args()
    return args

INTERVAL_STYLES = {"harmonic", "melodic", "cross"}
BACH_CHORALES = music21.corpus.getBachChorales()
parts = ["s", "a", "t", "b"]

def intervals(n, corpus=BACH_CHORALES, styles=INTERVAL_STYLES):
    """Return DataFrame with a row for each file in corpus."""

    if n is not None:
        corpus = corpus[:n]
    def parse(score):
        return music21.converter.parse(score).chordify(addPartIdAsGroup=True)
    return pd.concat((fts.reduce(pd.DataFrame.join,
                                 (globals()["process_" + style](parse(score))
                                  for style in styles))
                      for score in tqdm(corpus)),
                     ignore_index=True)

def pitch_dict(chord):
    result = {}
    s = {"Soprano", "SOPRANO", "soprano", "S"}
    a = {"Alto", "ALTO", "alto", "A"}
    t = {"Tenor", "TENOR", "tenor", "T"}
    b = {"Bass", "BASS", "bass", "B"}
    for pitch in chord.pitches:
        for group in pitch.groups:
            if group in s:
                result["s"] = pitch
            elif group in a:
                result["a"] = pitch
            elif group in t:
                result["t"] = pitch
            elif group in b:
                result["b"] = pitch
    return result

def diff_mod12(noteStart, noteEnd):
    return (music21
            .interval
            .Interval(noteStart=noteStart, noteEnd=noteEnd)
            .chromatic
            .mod12)

def process_harmonic(music):
    combos = [(parts[i], parts[j])
              for i in range(0, len(parts) - 1)
              for j in range(i + 1, len(parts))]
    def format_column(top, bottom, interval):
        return "h{}{}{:02}".format(top, bottom, interval)
    columns = [format_column(top, bottom, interval)
               for top, bottom in combos
               for interval in range(12)]
    data = pd.DataFrame(index=[0], columns=columns)
    data.fillna(0, inplace=True)

    chords = music.recurse().getElementsByClass("Chord")
    for chord in chords:
        pitches = pitch_dict(chord)
        if set(pitches.keys()) != set(parts):
            continue
        for top, bottom in combos:
            interval = diff_mod12(pitches[bottom], pitches[top])
            data[format_column(top, bottom, interval)][0] += 1
    return data

def process_cross(music):
    combos = [(left, right)
              for left in parts
              for right in parts
              if left != right]
    def format_column(left, right, interval):
        return "c{}{}{:02}".format(left, right, interval)
    columns = [format_column(left, right, interval)
               for left, right in combos
               for interval in range(12)]
    data = pd.DataFrame(index=[0], columns=columns)
    data.fillna(0, inplace=True)

    chords_from_0 = music.recurse().getElementsByClass("Chord")
    chords_from_1 = music.recurse().getElementsByClass("Chord")
    next(chords_from_1)
    for left_chord, right_chord in zip(chords_from_0, chords_from_1):
        left_pitches = pitch_dict(left_chord)
        right_pitches = pitch_dict(right_chord)
        if (set(left_pitches.keys()) != set(parts) or
            set(right_pitches.keys()) != set(parts)):
            continue
        for left_part, right_part in combos:
            interval = diff_mod12(left_pitches[left_part],
                                  right_pitches[right_part])
            data[format_column(left_part, right_part, interval)][0] += 1
    return data

def process_melodic(music):
    def format_column(part, interval):
        return "m{}{:02}".format(part, interval)
    columns = [format_column(part, interval)
               for part in parts for interval in range(12)]
    data = pd.DataFrame(index=[0], columns=columns)
    data.fillna(0, inplace=True)

    chords_from_0 = music.recurse().getElementsByClass("Chord")
    chords_from_1 = music.recurse().getElementsByClass("Chord")
    next(chords_from_1)
    for left_chord, right_chord in zip(chords_from_0, chords_from_1):
        left_pitches = pitch_dict(left_chord)
        right_pitches = pitch_dict(right_chord)
        if (set(left_pitches.keys()) != set(parts) or
            set(right_pitches.keys()) != set(parts)):
            continue
        for part in parts:
            interval = diff_mod12(left_pitches[part], right_pitches[part])
            data[format_column(part, interval)][0] += 1
    return data
